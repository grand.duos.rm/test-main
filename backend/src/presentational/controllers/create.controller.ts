import { ICreateUsecase } from './../../../../../../migra-o-processo-de-matricula-digital/backend/src/domain/usecases/create';
import { ISchemaValidator } from "../helpers/schemaValidator.helper";
import { BadRequest, Created, ExceptionError } from '../helpers/httpStatus.helper'; 

export class CreateController<In, Out> implements IController<In> {
  constructor(
    private readonly schemaValidator: ISchemaValidator<In>,
    private readonly createUsecase: ICreateUsecase<In, Out>
  ) {}
  public async handle(data: In): Promise<IHttpStatus<unknown>> {
    try {
      const maybeSchemaValidateError = this.schemaValidator.isValid(data);
      if (maybeSchemaValidateError) return BadRequest(maybeSchemaValidateError);
      const createdData = await this.createUsecase.execute(data);
      return Created(createdData);
    } catch (error) {
      return ExceptionError(error);
    }
  }
}
