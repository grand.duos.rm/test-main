import { ILoadWithPaginationUsecase } from "../../domain/usecases/loadWithPagination";
import { ExceptionError, Ok } from "../helpers/httpStatus.helper";

export class LoadWithPaginationController<In extends IPagination> {
  constructor(
    private readonly loadWithPaginationUsecase: ILoadWithPaginationUsecase<In>
  ) {}
  public async handle(data: In) {
    try {
      const items = await this.loadWithPaginationUsecase.execute({
        ...data,
        limit: Number(data.limit ),
        offset: Number(data.offset),
      });
      return Ok(items);
    } catch (error) {
      return ExceptionError(error);
    }
  }
}
