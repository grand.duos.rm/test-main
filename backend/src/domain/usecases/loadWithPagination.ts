export interface ILoadWithPaginationUsecase<T extends IPagination> {
  execute: (data: T) => Promise<T>;
}
