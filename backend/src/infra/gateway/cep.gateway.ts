import { IAddressService } from "../../data/protocols/addressService.service";
import { Address } from "../../domain/entities/address.entity";
import { CacheRepository } from "../repositories/cache";
import { ApiCepService } from "../service/apiCep.service";
import { ViaCepApiService } from "../service/viaCep.service";

export class CepGateway implements IAddressService {
  constructor(
    private readonly apiCep: ApiCepService,
    private readonly viaCep: ViaCepApiService,
    private readonly cepCache: CacheRepository
  ) {}

  public async loadByCep(cep: string): Promise<Address | null> {
    let maybeAddress = await this.cepCache.getItem<Address>("cep");
    if (maybeAddress) return maybeAddress;
    if (!maybeAddress) maybeAddress = await this.getViaCep(cep);
    if (!maybeAddress) maybeAddress = await this.getApiCep(cep);
    if (maybeAddress) await this.cepCache.setItem("cep", maybeAddress);
    return maybeAddress;
  }

  private async getApiCep(cep: string): Promise<Address | null> {
    const item = await this.apiCep.loadByCep(cep);
    if (!item) return null;
    return {
      cep,
      city: item.city,
      neighborhood: item.district,
      state: item.state,
      street: item.address,
    };
  }

  private async getViaCep(cep: string): Promise<Address | null> {
    const item = await this.viaCep.loadByCep(cep);
    if (!item) return null;
    return {
      cep,
      city: item.localidade,
      state: item.uf,
      neighborhood: item.bairro,
      street: item.logradouro,
    };
  }
}
