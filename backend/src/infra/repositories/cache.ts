import client from "../../main/config/cachDb";

export class CacheRepository {
  public async getItem<T>(key: string) {
    const cachedItem = await client.get(key);
    if (cachedItem) return JSON.parse(cachedItem) as T;
    return null;
  }

  public async setItem<T>(key: string, item: T) {
    const cached = await client.set(key, JSON.stringify(item));
    return cached;
  }
}
