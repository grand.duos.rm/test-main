# Teste Técnico - Desenvolvimento de API em Node/TypeScript

_Desafio técnico para o processo seletivo SQAD_<br>
_Documento técnico: [Link da documentação](https://docs.google.com/document/d/1PvGG43_N-ubZh9ccR-4gojnhivy9lb8B/edit)_<br>
_Autor: Rafael Medeiros_<br>

 ## Tecnologia

- Typescript + NodeJS + Express
- MondoDB + Redis
- Docker
- Nginx Web Server


## Pré-requisitos
Antes de começar, certifique-se de ter os seguintes pré-requisitos instalados:

- Docker
- Docker Compose
- Node.js (se você precisar executar testes localmente) - Node.js 18


## Execução

Clone este repositório GitLab:


> cd test-main<br>
> docker compose up



## Extra

Ele também inclui instruções para execução de testes unitários com yarn run test.


## Considerações

Implementação de testes unitários para garantir a qualidade do código.<br>
Princípios da Clean Architecture e Clean Code para manter o código organizado e legível.<br>
Disponibilização da documentação e modelagem da API endpoints via Swagger endpoint /api-docs/;<br>